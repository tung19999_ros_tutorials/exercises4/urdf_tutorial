#include <ros/ros.h>
#include <sensor_msgs/JointState.h>
#include <geometry_msgs/Twist.h>
#include <urdf_tutorial/changescale.h>
#include <urdf_tutorial/changejoint2.h>
#include <std_srvs/Empty.h>
#include <tf2_ros/transform_listener.h>
#include <geometry_msgs/TransformStamped.h>

double deltax,deltax2;
double scale = 1.0;         // default scale
double x[11];               // variable to control joint
int xmin[11] = {0,-150,-67,-150,-92,-150,-92,-150,0,-25,0}; // min of joint
int xmax[11] = {0,114,114,41,110,150,113,150,25,0,0};       // max of joint
double newScale;
int newJoint=2;             // joint change, default joint =2 is shoulder_pan_joint,
int newJoint2=3;            // following the table joint number in Session 6;

bool scaleChanged = false;
bool jointChanged = false;

const double degree2rad = M_PI/180;


bool changeScale(urdf_tutorial::changescale::Request &req,
                 urdf_tutorial::changescale::Response &resp){
    ROS_INFO_STREAM("Changing scale to" <<req.s);

    newScale = req.s;
    scaleChanged = true;
    return true;
}

bool changeJoint(urdf_tutorial::changejoint2::Request &req,
                 urdf_tutorial::changejoint2::Response &resp){
    ROS_INFO_STREAM("Changing joint to joint: " <<req.joint << "and joint: " << req.joint2 );

    newJoint = req.joint;
    newJoint2 = req.joint2;
    jointChanged = true;
    return true;
}

void teleopMessageReceived( const geometry_msgs::Twist& msg){
    // calculate angle
    deltax= msg.linear.x * degree2rad * scale;
    deltax2= msg.angular.z * degree2rad * scale;
    x[newJoint-1] = x[newJoint-1] + deltax;
    x[newJoint2-1] = x[newJoint2-1] + deltax2;

    if (x[newJoint-1] >= xmax[newJoint-1]*degree2rad){
        x[newJoint-1] = xmax[newJoint-1]*degree2rad;
        ROS_WARN_STREAM(std::setprecision(2) << std::fixed
       << "Limit max joint: " << xmax[newJoint-1]);
    }

    if (x[newJoint-1] <= xmin[newJoint-1]*degree2rad){
        x[newJoint-1] = xmin[newJoint-1]*degree2rad;
        ROS_WARN_STREAM(std::setprecision(2) << std::fixed
       << "Limit min joint: " << xmin[newJoint-1]);
    }

    if (x[newJoint2-1] >= xmax[newJoint2-1]*degree2rad){
        x[newJoint2-1] = xmax[newJoint2-1]*degree2rad;
        ROS_WARN_STREAM(std::setprecision(2) << std::fixed
       << "Limit max joint: " << xmax[newJoint2-1]);
    }

    if (x[newJoint2-1] <= xmin[newJoint2-1]*degree2rad){
        x[newJoint2-1] = xmin[newJoint2-1]*degree2rad;
        ROS_WARN_STREAM(std::setprecision(2) << std::fixed
       << "Limit min joint: " << xmin[newJoint2-1]);
    }

    ROS_INFO_STREAM(std::setprecision(2) << std::fixed
    << "Joint 1 " << newJoint << " angle: " << x[newJoint-1]/degree2rad
    << "Joint 2 " << newJoint2 << " angle: " << x[newJoint2-1]/degree2rad);
}



int main(int argc, char **argv)
{
  ros::init(argc, argv, "urdf_tutorial_arm3");
  ros::NodeHandle n;

  //The node advertises the joint values of the Elbow_pitch-Wrist_pitch
  ros::Publisher joint_pub = n.advertise<sensor_msgs::JointState>("joint_states", 1);

  // Subscribers teleop values
  ros::Subscriber sub = n.subscribe("teleop_values", 1000, &teleopMessageReceived);

  // Service change scale
  ros::ServiceServer server = n.advertiseService("change_scale",&changeScale);

  // Service change joint
  ros::ServiceServer server1 = n.advertiseService("change_joint",&changeJoint);

  sensor_msgs::JointState joint_state;
  // message declarations
  joint_state.name.resize(11);
  joint_state.position.resize(11);

  // tf2 buffer
  tf2_ros::Buffer tfBuffer;
  tf2_ros::TransformListener tfListener(tfBuffer);

  while (ros::ok())
  {
      geometry_msgs::TransformStamped transformStamped;

      //update joint_state
      joint_state.header.stamp = ros::Time::now();

      joint_state.name[0] ="bottom_joint";
      joint_state.name[1] ="shoulder_pan_joint";
      joint_state.name[2] ="shoulder_pitch_joint";
      joint_state.name[3] ="elbow_roll_joint";
      joint_state.name[4] ="elbow_pitch_joint";
      joint_state.name[5] ="wrist_roll_joint";
      joint_state.name[6] ="wrist_pitch_joint";
      joint_state.name[7] ="gripper_roll_joint";
      joint_state.name[8] ="finger_joint1";
      joint_state.name[9] ="finger_joint2";
      joint_state.name[10] ="grasping_frame_joint";
      joint_state.position[newJoint-1] = x[newJoint-1];
      joint_state.position[newJoint2-1] = x[newJoint2-1];
      //send the joint state
      joint_pub.publish(joint_state);

      try{
        transformStamped = tfBuffer.lookupTransform("base_link", "grasping_frame",
                                 ros::Time(0));
      }
      catch (tf2::TransformException &ex) {
        ROS_WARN("%s",ex.what());
        ros::Duration(1.0).sleep();
        continue;
      }

      if (transformStamped.transform.translation.z <=0.3){
          scale = 0.2; // decrease scale if z of grasping_frame<=0.3
          ROS_WARN_STREAM(std::setprecision(2) << std::fixed
         << "z Grasping below 0.3m, slow down ");
      }

      else {
          scale = 1.0;
      }
      ros::spinOnce();
      if(scaleChanged){
          scale = newScale;
          scaleChanged = false;
      }

      if(jointChanged){
          jointChanged = false;
      }

      ros::spinOnce();
  }
  return(0);
}



