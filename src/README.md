Ex4, Session 6: Tools. Urdf Tutorial

Ex4a:

    - Modify the code of the urdf_tutorial_template.cpp file to add a subscriber to a topic called teleop_values
    in order to teleoperate the pan and tilt degrees of freedom by using the arrow keys, which will be controlled
    by the turtlesim/teleop_turtle node.

    - Save the code as urdf_tutorial_a.cpp, and name the executable urdf_tutorial_a.

Ex4b:

    - Add a service to change the scale (call it urdf_tutorial/change_scale) in order to teleoperate the pan-tilt
    structure in a more fine or coarse way.

    - Save the code as urdf_tutorial_b.cpp, and name the executable urdf_tutorial_b.

Ex4c:

    - Change the pan-tilt structure by the 7-dof robot. Teleoperate joints elbow_pitch_joint and wrist_pitch_joint.

    - Save the code as urdf_tutorial_arm.cpp, and name the executable urdf_tutorial_arm.

Ex4d:

    - Add a service to choose which of the joints are to be teleoperated (you can neglect the prismatic joints of
    the fingers, or optionally consider them). Consider the limits of the joints given above.

    - Save the code as urdf_tutorial_arm2.cpp, and name the executable urdf_tutorial_arm2.

Ex4e:

    - Add a TransformListener to monitorize the z value of the grasping_frame and slow down the teleoperation when
    its value is below 0.3 m.

    - Save the code as urdf_tutorial_arm3.cpp, and name the executable urdf_tutorial_arm3.

Ex4f:

    - Add an object in front of the robot, e.g. the UpperBody part of the plane, and attach a static transform to it.
    To do this modify the rviz_node.cpp; call it rviz_node2.cpp. Then, monitorize the distance from the grasping_frame
    to the object frame and slow down the teleoperation whenever this distance is below 0.5 m.

    - You can draw a sphere to visualize the zone where the teleoperation is slowed down.

    - Save the code as urdf_tutorial_arm4.cpp, and name the executable urdf_tutorial_arm4. For this example you can
    create a new launch file that also has to run the rviz_node2 node.
