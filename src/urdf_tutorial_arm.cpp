#include <ros/ros.h>
#include <sensor_msgs/JointState.h>
#include <geometry_msgs/Twist.h>
#include <urdf_tutorial/changescale.h>
#include <std_srvs/Empty.h>

double deltaElbow_pitch;
double deltaWrist_pitch;
double scale = 0.5;         // default scale
double Elbow_pitch = 0.0;
double Wrist_pitch = 0.0;
double newScale;
bool scaleChanged = false;

const double degree2rad = M_PI/180;

bool changeScale(urdf_tutorial::changescale::Request &req,
                 urdf_tutorial::changescale::Response &resp){
    ROS_INFO_STREAM("Changing scale to" <<req.s);

    newScale = req.s;
    scaleChanged = true;
    return true;
}

void teleopMessageReceived( const geometry_msgs::Twist& msg){


    // calculate angle
    deltaElbow_pitch = msg.angular.z * degree2rad * scale;
    deltaWrist_pitch = msg.linear.x * degree2rad * scale;

    Elbow_pitch = Elbow_pitch + deltaElbow_pitch;
    Wrist_pitch = Wrist_pitch + deltaWrist_pitch;

//    ROS_INFO_STREAM(std::setprecision(2) << std::fixed
//        << "Elbow_pitch=" << Elbow_pitch << ","
//        << " Wrist_pitch=" << Wrist_pitch);
}



int main(int argc, char **argv)
{
  ros::init(argc, argv, "urdf_tutorial_arm");
  ros::NodeHandle n;

  //The node advertises the joint values of the Elbow_pitch-Wrist_pitch
  ros::Publisher joint_pub = n.advertise<sensor_msgs::JointState>("joint_states", 1);

  // Subscribers teleop values
  ros::Subscriber sub = n.subscribe("teleop_values", 1000, &teleopMessageReceived);

  // Service change scale
  ros::ServiceServer server = n.advertiseService("change_scale",&changeScale);

  sensor_msgs::JointState joint_state;
  // message declarations
  joint_state.name.resize(11);
  joint_state.position.resize(11);

  while (ros::ok())
  {
      //update joint_state
      joint_state.header.stamp = ros::Time::now();

      joint_state.name[0] ="bottom_joint";
      joint_state.position[0] = 0;
      joint_state.name[1] ="shoulder_pan_joint";
      joint_state.position[1] = 0;
      joint_state.name[2] ="shoulder_pitch_joint";
      joint_state.position[2] = 0;
      joint_state.name[3] ="elbow_roll_joint";
      joint_state.position[3] = 0;
      joint_state.name[4] ="elbow_pitch_joint";
      joint_state.position[4] = Elbow_pitch;
      joint_state.name[5] ="wrist_roll_joint";
      joint_state.position[5] = 0;
      joint_state.name[6] ="wrist_pitch_joint";
      joint_state.position[6] = Wrist_pitch;
      joint_state.name[7] ="gripper_roll_joint";
      joint_state.position[7] = 0;
      joint_state.name[8] ="finger_joint1";
      joint_state.position[8] = 0;
      joint_state.name[9] ="finger_joint2";
      joint_state.position[9] = 0;
      joint_state.name[10] ="grasping_frame_joint";
      joint_state.position[10] = 0;
      //send the joint state
      joint_pub.publish(joint_state);

      ros::spinOnce();
      if(scaleChanged){
          scale = newScale;
          scaleChanged = false;
      }
  }
  return(0);
}



