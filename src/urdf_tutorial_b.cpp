#include <ros/ros.h>
#include <sensor_msgs/JointState.h>
#include <geometry_msgs/Twist.h>
#include <urdf_tutorial/changescale.h>
#include <std_srvs/Empty.h>

double deltaPan;
double deltaTilt;
double scale = 0.5; // default
double pan = 0.0;
double tilt = 0.0;
double newScale;
bool scaleChanged = false;

const double degree2rad = M_PI/180;

bool changeScale(urdf_tutorial::changescale::Request &req,
                 urdf_tutorial::changescale::Response &resp){
    ROS_INFO_STREAM("Changing scale to" <<req.s);

    newScale = req.s;
    scaleChanged = true;
    return true;
}

void teleopMessageReceived( const geometry_msgs::Twist& msg){


    // calculate angle
    deltaPan = msg.angular.z * degree2rad * scale;
    deltaTilt = msg.linear.x * degree2rad * scale;

    pan = pan + deltaPan;
    tilt = tilt + deltaTilt;

//    ROS_INFO_STREAM(std::setprecision(2) << std::fixed
//        << "Pan=" << pan << ","
//        << " Tilt=" << tilt);
}



int main(int argc, char **argv)
{
  ros::init(argc, argv, "urdf_tutorial_a");
  ros::NodeHandle n;

  //The node advertises the joint values of the pan-tilt
  ros::Publisher joint_pub = n.advertise<sensor_msgs::JointState>("joint_states", 1);

  // Subscribers teleop values
  ros::Subscriber sub = n.subscribe("teleop_values", 1000, &teleopMessageReceived);

  // Service change scale
  ros::ServiceServer server = n.advertiseService("change_scale",&changeScale);

  sensor_msgs::JointState joint_state;
  // message declarations
  joint_state.name.resize(2);
  joint_state.position.resize(2);

  while (ros::ok())
  {
      //update joint_state
      joint_state.header.stamp = ros::Time::now();
      joint_state.name[0] ="pan_joint";
      joint_state.position[0] = pan;
      joint_state.name[1] ="tilt_joint";
      joint_state.position[1] = tilt;

      //send the joint state
      joint_pub.publish(joint_state);

      ros::spinOnce();
      if(scaleChanged){
          scale = newScale;
          scaleChanged = false;
      }
  }
  return(0);
}



