#include <ros/ros.h>
#include <sensor_msgs/JointState.h>
#include <geometry_msgs/Twist.h>

double deltaPan;
double deltaTilt;
double scale;
double pan = 0.0;
double tilt = 0.0;
const double degree2rad = M_PI/180;


void teleopMessageReceived( const geometry_msgs::Twist& msg){

    scale =0.5;
    // calculate angle
    deltaPan = msg.angular.z * degree2rad * scale;
    deltaTilt = msg.linear.x * degree2rad * scale;

    pan = pan + deltaPan;
    tilt = tilt + deltaTilt;

//    ROS_INFO_STREAM(std::setprecision(2) << std::fixed
//        << "Pan=" << pan << ","
//        << " Tilt=" << tilt);
}



int main(int argc, char **argv)
{
  ros::init(argc, argv, "urdf_tutorial_a");
  ros::NodeHandle n;

  //The node advertises the joint values of the pan-tilt
  ros::Publisher joint_pub = n.advertise<sensor_msgs::JointState>("joint_states", 1);
  ros::Subscriber sub = n.subscribe("teleop_values", 1000, &teleopMessageReceived);

  sensor_msgs::JointState joint_state;
  // message declarations
  joint_state.name.resize(2);
  joint_state.position.resize(2);

  while (ros::ok())
  {
      //update joint_state
      joint_state.header.stamp = ros::Time::now();
      joint_state.name[0] ="pan_joint";
      joint_state.position[0] = pan;
      joint_state.name[1] ="tilt_joint";
      joint_state.position[1] = tilt;

      //send the joint state
      joint_pub.publish(joint_state);

      ros::spinOnce();
  }
  return(0);
}



