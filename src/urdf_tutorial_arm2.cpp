#include <ros/ros.h>
#include <sensor_msgs/JointState.h>
#include <geometry_msgs/Twist.h>
#include <urdf_tutorial/changescale.h>
#include <urdf_tutorial/changejoint.h>
#include <std_srvs/Empty.h>

double deltax;
double scale = 0.5;         // default scale
double x[11];               // variable to control joint
int xmin[11] = {0,-150,-67,-150,-92,-150,-92,-150,0,-25,0}; // min of joint
int xmax[11] = {0,114,114,41,110,150,113,150,25,0,0};       // max of joint
double newScale;
int newJoint=2;             // joint change, default joint =2 is shoulder_pan_joint,
                            // following the table joint number in Session 6;

bool scaleChanged = false;
bool jointChanged = false;

const double degree2rad = M_PI/180;


bool changeScale(urdf_tutorial::changescale::Request &req,
                 urdf_tutorial::changescale::Response &resp){
    ROS_INFO_STREAM("Changing scale to" <<req.s);

    newScale = req.s;
    scaleChanged = true;
    return true;
}

bool changeJoint(urdf_tutorial::changejoint::Request &req,
                 urdf_tutorial::changejoint::Response &resp){
    ROS_INFO_STREAM("Changing joint to" <<req.joint);

    newJoint = req.joint;
    jointChanged = true;
    return true;
}

void teleopMessageReceived( const geometry_msgs::Twist& msg){


    // calculate angle
    deltax= msg.linear.x * degree2rad * scale;
    x[newJoint-1] = x[newJoint-1] + deltax;

    if (x[newJoint-1] >= xmax[newJoint-1]*degree2rad){
        x[newJoint-1] = xmax[newJoint-1]*degree2rad;
        ROS_WARN_STREAM(std::setprecision(2) << std::fixed
       << "Limit max joint: " << xmax[newJoint-1]);
    }

    if (x[newJoint-1] <= xmin[newJoint-1]*degree2rad){
        x[newJoint-1] = xmin[newJoint-1]*degree2rad;
        ROS_WARN_STREAM(std::setprecision(2) << std::fixed
       << "Limit min joint: " << xmin[newJoint-1]);
    }
    ROS_INFO_STREAM(std::setprecision(2) << std::fixed
    << "Joint " << newJoint-1 << " angle: " << x[newJoint-1]/degree2rad);
}



int main(int argc, char **argv)
{
  ros::init(argc, argv, "urdf_tutorial_arm2");
  ros::NodeHandle n;

  //The node advertises the joint values of the Elbow_pitch-Wrist_pitch
  ros::Publisher joint_pub = n.advertise<sensor_msgs::JointState>("joint_states", 1);

  // Subscribers teleop values
  ros::Subscriber sub = n.subscribe("teleop_values", 1000, &teleopMessageReceived);

  // Service change scale
  ros::ServiceServer server = n.advertiseService("change_scale",&changeScale);

  // Service change joint
  ros::ServiceServer server1 = n.advertiseService("change_joint",&changeJoint);

  sensor_msgs::JointState joint_state;
  // message declarations
  joint_state.name.resize(11);
  joint_state.position.resize(11);

  while (ros::ok())
  {
      //update joint_state
      joint_state.header.stamp = ros::Time::now();

      joint_state.name[0] ="bottom_joint";
      joint_state.name[1] ="shoulder_pan_joint";
      joint_state.name[2] ="shoulder_pitch_joint";
      joint_state.name[3] ="elbow_roll_joint";
      joint_state.name[4] ="elbow_pitch_joint";
      joint_state.name[5] ="wrist_roll_joint";
      joint_state.name[6] ="wrist_pitch_joint";
      joint_state.name[7] ="gripper_roll_joint";
      joint_state.name[8] ="finger_joint1";
      joint_state.name[9] ="finger_joint2";
      joint_state.name[10] ="grasping_frame_joint";
      joint_state.position[newJoint-1] = x[newJoint-1];
      //send the joint state
      joint_pub.publish(joint_state);

      ros::spinOnce();
      if(scaleChanged){
          scale = newScale;
          scaleChanged = false;
      }

      if(jointChanged){
          jointChanged = false;
      }
  }
  return(0);
}



